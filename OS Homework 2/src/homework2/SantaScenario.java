/*
 * Robert Max Gomez
 * COP 4600
 * Homework 2
 * 
 * STEP 5
 * 
 */

package homework2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SantaScenario {

	public Santa santa;
	public List<Elf> elves;
	public List<Reindeer> reindeers;
	public boolean isDecember;
	public static ArrayList<Integer> elvesInTrouble = new ArrayList<Integer>();
	public static ArrayList<Semaphore> deerSem = new ArrayList<Semaphore>();
	
	static int temp, reindeerCount = 0;
	
	//this semaphore only allows one elf to run at a time
	public Semaphore waitElf = new Semaphore(1);

	static boolean counter = true;

	public static void main(String args[]) {
		
		//initialize 9 semaphores for the 9 reindeer
		for(int i = 0; i < 9; i++)
			deerSem.add(new Semaphore(1));
		
		
		SantaScenario scenario = new SantaScenario();
		scenario.isDecember = false;
		// create the participants

		// Santa
		scenario.santa = new Santa(scenario);
		Thread th = new Thread(scenario.santa);
		th.start();

		// The elves: in this case: 10
		scenario.elves = new ArrayList<>();
		for (int i = 0; i != 10; i++) {
			Elf elf = new Elf(i + 1, scenario);
			scenario.elves.add(elf);
			th = new Thread(elf);
			th.start();
		}

		 // The reindeer: in this case: 9
		 scenario.reindeers = new ArrayList<>();
		 for (int i = 0; i != 9; i++) {
		 Reindeer reindeer = new Reindeer(i + 1, scenario);
		 scenario.reindeers.add(reindeer);
		 th = new Thread(reindeer);
		 th.start();
		 }

		// now, start the passing of time
		for (int day = 300; day < 380; day++) {

			// wait a day
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// turn on December
			if (day > (365 - 31)) {
				scenario.isDecember = true;
			}

			if (day == 370)
				counter = false;

			// print out the state:
			System.out.println("***********  Day " + day + " *************************");
			scenario.santa.report();

			for (Elf elf : scenario.elves) {
				elf.report();
			}

			for (Reindeer reindeer : scenario.reindeers) {
				reindeer.report();
			}
		}
	}

}