/*
 * Robert Max Gomez
 * COP 4600
 * Homework 2
 * 
 * STEP 5
 * 
 */

package homework2;

import homework2.Elf.ElfState;
import homework2.Reindeer.ReindeerState;

public class Santa implements Runnable {

	enum SantaState {
		SLEEPING, READY_FOR_CHRISTMAS, WOKEN_UP_BY_ELVES, WOKEN_UP_BY_REINDEER
	};
	
	private SantaScenario scenario;
	private SantaState state;
	private Elf elf = new Elf();
	

	public Santa() {
		
	}

	public Santa(SantaScenario scenario) {
		this.state = SantaState.SLEEPING;
		this.scenario = scenario;
	}

	public SantaState getState() {
		return state;
	}


	public void setState(SantaState state) {
		this.state = state;
	}

	@Override
	public void run() {
		while (scenario.counter) {
			
			// wait a day...
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			switch (state) {
			case SLEEPING: // if sleeping, continue to sleep
				break;
			case WOKEN_UP_BY_ELVES:
				// FIXME: help the elves who are at the door and go back to
				
				//if an elf is at the door while santa is awake, fix their problems and put them back to work
				for(int i=0; i < 10; i++){
					if(scenario.elves.get(i).getState() == ElfState.AT_SANTAS_DOOR)
						scenario.elves.get(i).setState(ElfState.WORKING);
				}

					state = SantaState.SLEEPING;
				
				// sleep
				break;
			case WOKEN_UP_BY_REINDEER:
				// FIXME: assemble the reindeer to the sleigh then change state to ready
				
				//remove lock from reindeer thread and move them to the sleigh
				for(int i = 0; i < 9; i++){
					scenario.deerSem.get(i).release();
					scenario.reindeers.get(i).setState(ReindeerState.AT_THE_SLEIGH);
				}
				
				//Santa is now ready for Christmas
				state = SantaState.READY_FOR_CHRISTMAS;
				
				break;
			case READY_FOR_CHRISTMAS: // nothing more to be done
				break;
			}
		}
	}

	/**
	 * Report about my state
	 */
	public void report() {
		System.out.println("Santa : " + state);
	}

}