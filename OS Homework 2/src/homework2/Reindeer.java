/*
 * Robert Max Gomez
 * COP 4600
 * Homework 2
 * 
 * STEP 4
 * 
 */

package homework2;

import homework2.Santa.SantaState;

import java.util.Random;

public class Reindeer implements Runnable {

	public enum ReindeerState {
		AT_BEACH, AT_WARMING_SHED, AT_THE_SLEIGH
	};

	private ReindeerState state;
	private SantaScenario scenario;
	private Random rand = new Random();

	/**
	 * The number associated with the reindeer
	 */
	private int number;

	public Reindeer(int number, SantaScenario scenario) {
		this.number = number;
		this.scenario = scenario;
		this.state = ReindeerState.AT_BEACH;
	}

	public ReindeerState getState() {
		return state;
	}

	public void setState(ReindeerState state) {
		this.state = state;
	}

	@Override
	public void run() {

		while (scenario.counter) {
			// wait a day
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			// see what we need to do:
			switch (state) {
			case AT_BEACH: { // if it is December, the reindeer might think
								// about returning from the beach
				if (scenario.isDecember) {
					if (rand.nextDouble() < 0.1) {
						state = ReindeerState.AT_WARMING_SHED;
					}
				}
				break;
			}
			case AT_WARMING_SHED:
				// if all the reindeer are home, wake up santa
				
				if (scenario.reindeerCount < 9) {
					try {
						scenario.deerSem.get(number-1).acquire();
						scenario.reindeerCount++;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else{
					//9th reindeer wakes up santa and moves to the warming shed
					state = ReindeerState.AT_WARMING_SHED;
					scenario.santa.setState(SantaState.WOKEN_UP_BY_REINDEER);
				}
				break;
			case AT_THE_SLEIGH:
				// keep pulling
				
				
				break;
			}
		}
	};

	/**
	 * Report about my state
	 */
	public void report() {
		System.out.println("Reindeer " + number + " : " + state);
	}

}
