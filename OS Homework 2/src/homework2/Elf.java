/*
 * Robert Max Gomez
 * COP 4600
 * Homework 2
 * 
 * STEP 5
 * 
 */

package homework2;

import homework2.Santa.SantaState;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Elf implements Runnable {

	enum ElfState {
		WORKING, TROUBLE, AT_SANTAS_DOOR
	};

	private ElfState state;
	/**
	 * The number associated with the Elf
	 */
	private int number, temp;
	private Random rand = new Random();
	private SantaScenario scenario;
//	public static Semaphore wait = new Semaphore(1);
	
	
	ArrayList<Integer> elvesInTrouble = new ArrayList<Integer>();

	public Elf(int number, SantaScenario scenario) {
		this.number = number;
		this.scenario = scenario;
		this.state = ElfState.WORKING;
	}

	public Elf() {

	}

	public ElfState getState() {
		return state;
	}

	/**
	 * Santa might call this function to fix the trouble
	 * 
	 * @param state
	 */
	public void setState(ElfState state) {
		this.state = state;
	}

	@Override
	public void run() {

		while (scenario.counter) {

			// wait a day
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			switch (state) {
			case WORKING: {
				// at each day, there is a 1% chance that an elf runs into trouble.
				if (rand.nextDouble() < 0.01) {
					state = ElfState.TROUBLE;
				}
				break;
			}
			case TROUBLE:
				// FIXME: if possible, move to Santa's door
				try {
					scenario.waitElf.acquire();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//if there are less than 3 elves in trouble, or if it doesnt exist in the list yet
				if(!scenario.elvesInTrouble.contains(number) && scenario.elvesInTrouble.size() < 3)
					scenario.elvesInTrouble.add(number);
				
				//need this if because after the first iteration of the while loop
				//there may not be 3 elfs in trouble anymore
				if (scenario.elvesInTrouble.size() == 3) {
					for (int i = 0; i < 3; i++) {
						// correct off by one error by subtracting one because elves array starts at index 0 and elf number starts at 1
						temp = scenario.elvesInTrouble.get(i) - 1;
						scenario.elves.get(temp).setState(ElfState.AT_SANTAS_DOOR);
					}
					scenario.elvesInTrouble.clear();
				}
				
				scenario.waitElf.release();
				
//				state = ElfState.AT_SANTAS_DOOR;
				break;
			case AT_SANTAS_DOOR:
				// FIXME: if feasible, wake up Santa
				if (scenario.santa.getState() == SantaState.SLEEPING)
					scenario.santa.setState(SantaState.WOKEN_UP_BY_ELVES);
				break;
			}
		}
	}

	/**
	 * Report about my state
	 */
	public void report() {
		System.out.println("Elf " + number + " : " + state);
	}

}